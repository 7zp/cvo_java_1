/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;

import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author Gilles
 */
public class Oefening3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        int ran, input = 0, poging = 1, verschil = 0, ronde = 1, gewonnen =0;
        String titel;
        System.out.println("Ik denk aan een getal tussen 1 en 10 (inbegrepen)");
        System.out.println("Je moet dit getal raden met maximaal 3 pogingen");
        
        for (;;) {
            ran = rand.nextInt(9) + 1;
            poging = 1;
            
            System.out.format("Ronde %d\n", ronde);
            System.out.println("Doe een gok:");
            for (;;) {
                input = scan.nextInt();
                if (ran == input) {
                    System.out.println("JUIST!");
                    System.out.format("Je hebt het getal gevonden! Het was inderdaad %d\n", ran);
                    gewonnen++;
                    break;
                }
                if (poging == 3) {
                    System.out.format("Het correcte getal was %d\n", ran);
                    System.out.println("Je hebt het spel verloren");
                    break;
                }
                if (input > ran) {
                    verschil = input - ran;
                } else {
                    verschil = ran - input;
                }
                if (verschil > 2) {
                    System.out.println("Koud");
                } else if (verschil > 1) {
                    System.out.println("Warm");
                } else {
                    System.out.println("Heet");
                }
                System.out.println("Doe nog een gok!");
                poging++;
            }
            if (ronde == 10){
                if (gewonnen == 10)
                    titel = "hacker!";
                else if (gewonnen == 9)
                    titel = "professional";
                else if (gewonnen == 8)
                    titel = "gevorderde";
                else 
                    titel = "amateur";
                
                System.out.format("Je hebt %d rondes gewonnen.\nJij bent een %s\n", gewonnen, titel);
                break;
            }
            ronde++;
        }
    }

}
