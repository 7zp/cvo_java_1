/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        int ran, input = 0, poging = 1;
        ran = rand.nextInt(9)+1;
        System.out.println("Ik denk aan een getal tussen 1 en 10 (inbegrepen)");
        System.out.println("Je moet dit getal raden met maximaal 3 pogingen");
        System.out.println("Doe een gok:");
        for(;;){
            input = scan.nextInt();
            if(ran == input) {
                System.out.println("JUIS!");
                System.out.format("Je hebt het getal gevonden! Het was inderdaad %d\n", ran);
                break;
            }
            if (poging == 3){
                System.out.format("Het correcte getal was %d\n", ran);
                System.out.println("Je hebt het spel verloren");
                break;
            }
            System.out.println("Doe nog een gok!");
            poging++;
        }
    }
    
}
