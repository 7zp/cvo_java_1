/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening5;
import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Gilles
 */
public class Oefening5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        Random randGen = new Random();
        int ronde = 1, totaalComputer = 0, totaalSpeler = 0, tussenComputer = 0, tussenSpeler = 0, worp = 0;
        String input;
        for(;;){
            
            
            //Computer werpt
            tussenComputer = (randGen.nextInt(5)+ 1) +(randGen.nextInt(5)+ 1) +(randGen.nextInt(5)+ 1);
            totaalComputer += tussenComputer;
            System.out.format("De computer gooide in totaal %d\nDe computer heeft nu een totaal van %d\n", tussenComputer, totaalComputer);
            if (totaalComputer >= 100)
                break;
            //speler
            worp = randGen.nextInt(5) +1;
            tussenSpeler = worp;
            for(;;){
                if (worp == 1){
                    tussenSpeler = 0;
                    System.out.println("Je wierp 1, einde van jouw ronde!");
                    break;
                }
                if (totaalSpeler + tussenSpeler >= 100)
                    break;
                System.out.format("Je wierp %d je hebt nu een totaal van %d\n", worp, tussenSpeler);
                //Nog eens werpen
                System.out.println("Nog een gooi? (y/n)");
                input = scan.nextLine();
                if (input.equals("n")){
                    System.out.println("Je gaf je beurt af.");
                    break;
                }
                worp = randGen.nextInt(5) +1;
                tussenSpeler += worp;
                
            }
            totaalSpeler += tussenSpeler;
            System.out.format("Je gooide in totaal %d\nDe computer heeft nu een totaal van %d\n", tussenSpeler, totaalSpeler);
            //end
            ronde++;
            
            System.out.format("################################\nRonde %d\nComputer %d\tJij %d\n################################\n", ronde, totaalComputer, totaalSpeler);
        }
        System.out.println("################################");
        if (totaalComputer >= 100)
            System.out.format("De computer won met %d in %d beurent\nJij had slechts %d\n", totaalComputer, ronde, totaalSpeler);
        else
            System.out.format("Jij won met %d in %d beurent\nDe computer had slechts %d\n", totaalSpeler, ronde, totaalComputer);
        
    }
    
}
