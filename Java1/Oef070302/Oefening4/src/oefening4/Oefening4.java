/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;
import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Gilles
 */
public class Oefening4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        Random randgen = new Random();
        String[] karakters = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String inputPasswoord, gok;
        int gokAantal = 0, gokLengte, gokLengteTemp;
        int lijstLen = karakters.length - 1;
        System.out.println("Voer een kort passwoord in (max 5 karakters) en dit progamma zal het gokken:");
        for(;;){
            inputPasswoord = scan.nextLine();
            if (inputPasswoord.length() > 5 || inputPasswoord.length() < 1){
                System.out.println("Het gegeven passwoord moet minstens 1 karakter bevatten en maximaal 5, probeer nogmaals:");
                continue;
            }
            break;
        }
        
        for(;;) {
            gokAantal++;
            gokLengte = randgen.nextInt(4)+1;
            gokLengteTemp = 0;
            gok = "";
            while ( gokLengteTemp < gokLengte){
                gok = gok + karakters[randgen.nextInt(lijstLen)];
                gokLengteTemp++;
            }
            if(gok.contentEquals(inputPasswoord))
                break;
        }
        System.out.format("Het passwoord was %s, geraden na %d gokken\n", gok, gokAantal);
    }
    
}
