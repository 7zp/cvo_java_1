/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int centInput, centOutput, euroOutput;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Voer heb bedrag in eurocent in:");
        centInput = scan.nextInt();
        
        //Euro
        euroOutput = centInput / 100;
        //Cent
        centOutput = centInput % 100;
        
        System.out.format("%d cent is %d euro en %d eurocent%n", centInput, euroOutput, centOutput);
        
    }
    
}
