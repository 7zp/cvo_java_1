/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OppervlakteCilinder;
import java.util.Scanner;

/**
 *
 * @author Gilles
 */
public class OppervlakteCilinder {

    public static void main(String[] args) {
        final double pi = 3.141519;
        double straal, hoogte, oppervlakte;
        Scanner scan = new Scanner(System.in);
        System.out.println("Voer de straal en de hoogte in, gescheiden door een spatie:");
        straal = scan.nextDouble();
        hoogte = scan.nextDouble();
        
        oppervlakte = (2 * pi * straal * (straal + hoogte));
        System.out.format("De oppervlakte van een cilinder met straal= %f en hoogte= %f is %f %n", straal, hoogte, oppervlakte);
    }
    
}
