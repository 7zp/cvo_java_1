/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;

import java.util.Scanner;
public class Oefening4 {
    public static void main(String[] args) {
        double amp, volt, ohm;
        Scanner scan = new Scanner(System.in);
        System.out.println("Geef de spanning en de weerstand in: ");
        volt = scan.nextDouble();
        ohm = scan.nextDouble();
        
        amp = volt / ohm;
        System.out.format("De stroomsterkte voor een spanning van %.0f Volt en een weerstand van %.0f Ohm is %.1f Apmère.%n", volt, ohm, amp);
    }
}
