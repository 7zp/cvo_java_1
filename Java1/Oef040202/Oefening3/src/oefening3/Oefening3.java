/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;

import java.util.Scanner;

public class Oefening3 {

    public static void main(String[] args) {
        int[] munten = {200, 100, 50, 20, 10, 5, 2, 1};
        int restant, stuk;
        String type, woord;
        Scanner scan = new Scanner(System.in);

        System.out.println("Geef het wisselgeld in centen in:");
        restant = scan.nextInt();
        //restant = 531; //Test
        
        System.out.format("%d cent is:%n", restant);
        
        for (int munt : munten) {
            stuk = restant / munt;      //Aantal stukken
            restant = restant % munt;   //Restant
            
            //Juiste munt keuze
            if (munt >= 100) {
                munt = munt / 100;
                type = "euro";
            } else {
                type = "cent";
            }
            
            //Stukken of stuk
            if (stuk > 1 || stuk == 0) {
                woord = "stukken";
            } else {
                woord = "stuk   ";  //Spaties voor betere weergave
            }
            System.out.format("%d %s van %d %s%n", stuk, woord, munt, type);
        }
    }

}
