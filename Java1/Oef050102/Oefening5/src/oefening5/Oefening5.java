/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening5;
import java.util.Scanner;

public class Oefening5 {
    public static void main(String[] args) {
           Scanner scan = new Scanner(System.in);
           int jaarGeboorte, jaarHuidig, leeftijd;
           
           System.out.println("Geef de laatste twee getallen van je geboortejaar op:");
           jaarGeboorte = scan.nextInt();
           System.out.println("Geef de laatste twee getallen van het huidig jaar op:");
           jaarHuidig = scan.nextInt();
           
           //Bereken
           jaarGeboorte += 1900;
           jaarHuidig += 2000;
           
           leeftijd = jaarHuidig - jaarGeboorte;
           if (leeftijd > 100)
               leeftijd -= 100;
           System.out.format("Je leeftijd is %d%n", leeftijd);         
    }
    
}
