/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;

import java.util.Scanner;

public class Oefening4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double inhoudMax, inhoudStand, verbruik;
        int resterendBereik;
        
        System.out.println("Vul achtereenvolgens in: inhoud tank, stand tank in percent, verbuik liter/100km:"); 

        inhoudMax = scan.nextInt();
        inhoudStand = scan.nextInt();
        verbruik = scan.nextInt();
        resterendBereik = (int)((((inhoudMax / 100) * inhoudStand) / verbruik) * 100);
        
        if (resterendBereik < 300 ){
            System.out.println("Tank maar voor alle veiligheid!");
        } else {
            System.out.println("OK, je haalt het wel!");
        }
    } 
}
