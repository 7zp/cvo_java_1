/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening6;
import java.util.Locale;
import java.util.Scanner;
public class Oefening6 {
    public static void main(String[] args) {
        double prijsKgA, prijsKgB, prijsA, prijsB;
        int vleesA, vleesB;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        
        System.out.println("Geef prijs per kg en percentage mager vlees van pakje A:");
        prijsKgA = scan.nextDouble();
        vleesA = scan.nextInt();
        System.out.println("Geef prjis per kg en percentage mager vlees van pakje B:");
        prijsKgB = scan.nextDouble();
        vleesB = scan.nextInt();
        
        prijsA = (prijsKgA / vleesA) * 100;
        prijsB = (prijsKgB / vleesB) * 100;
        
        System.out.format("Kostprijs mager vlees per kg pakje A is %f %n"
                        + "Kostprijs mager vlees per kg pakje B is %f %n", 
                           prijsA, prijsB);
        
        if (prijsA < prijsB)
            System.out.println("Pakje A is voordeliger");
        else
            System.out.println("Pakje B is voordeliger");     
    }   
}
