/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int bedrag;

        Scanner scan = new Scanner(System.in);
        
        System.out.println("Voer het bedrag in (in eurocent):");
        bedrag = scan.nextInt();
        
        if (bedrag > 1000){
            bedrag -= (bedrag / 10 );
        }
        System.out.format("Aankoopprijs min eventuele korting %d%n", bedrag);
    }
    
}
