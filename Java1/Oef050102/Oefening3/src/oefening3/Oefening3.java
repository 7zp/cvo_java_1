/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Scanner;

public class Oefening3 {

    public static void main(String[] args) {
        final int gewichtMin = 110, gewichtMax = 140;
        int gewichtInvoer;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Geef je gewicht in:");
        gewichtInvoer = scan.nextInt();
        
        if ( gewichtInvoer < gewichtMin){
            System.out.println("Je wordt NIET toegelaten tot de wedstrijd");
            System.out.println("Je bent te LICHT");
        } else if (gewichtInvoer > gewichtMax){
            System.out.println("Je wordt NIET toegelate tot de wedstrijd");
            System.out.println("Je bent te ZWAAR");
        } else {
            System.out.println("Je wordt toegelaten tot de wedstrijd");
        }
    }
    
}
