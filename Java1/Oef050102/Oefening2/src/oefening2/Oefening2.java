/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;

import java.util.Scanner;
public class Oefening2 {

    public static void main(String[] args) {
        boolean foutBouten = false, foutMoeren = false, foutRingen = false;
        int bouten, moeren, ringen, totaal;
        final int prijsBout = 5, prijsMoer = 5, prijsRing = 2;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Geef aantal bouten, moeren en ringen in, gescheiden door spaties:");
        
        bouten = scan.nextInt();
        moeren = scan.nextInt();
        ringen = scan.nextInt();
        
        if ( bouten > moeren) {
            //Te weinig moeren
            foutMoeren = true;
        }
        if ( moeren > bouten){
            //Te weinig bouten
            foutBouten = true;
        }
        if ( ringen < bouten*2 || ringen < moeren*2){
            //Te weinig ringen
            foutRingen = true;
        }
        if (foutBouten || foutMoeren || foutRingen){
            System.out.println("Bestelling NIET OK!");
            if (foutBouten)
                System.out.println("Te weinig bouten");
            if (foutMoeren)
                System.out.println("Te weinig moeren");
            if (foutRingen)
                System.out.println("Te weinig ringen");
        } else {
            System.out.println("Bestellign OK!");
        }
        
        totaal = (bouten * prijsBout) + (moeren * prijsMoer) + (ringen * prijsRing);
        
        System.out.format("De totale prijs van uw bestelling is %d eurocent%n", totaal);
    }    
}
