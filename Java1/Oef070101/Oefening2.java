/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        double begin, eind, verbruik;
        
        System.out.println("Voer een beginstand km teller in (-1 om te stoppen):");
        begin = scan.nextFloat();
        if (begin < 0)
        {
            System.out.println("Tot ziens!");
            return;
        }
        
        System.out.println("Voer eindstan km in:");
        eind = scan.nextFloat();
        System.out.println("Voer verbuik in liters in:");
        verbruik = scan.nextFloat();
        
        System.out.format("Verbruik ub liters/100 km is: %.4f\n", ( (verbruik / (eind-begin))* 100));
    }
    
}
