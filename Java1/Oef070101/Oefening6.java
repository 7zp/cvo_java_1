/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening6;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int index;
        String inputNaam, inputPasswoord;
        boolean exist = false;
        String[] gebruiker = {
            "jan", "piet", "jos", "frans", "stop"
        };
        String[] passwoord = {
            "appel", "kers", "banaan", "peer", "exit"
        };
        int[] prio = {
            1,2,3,4,5
        };
        
        for(;;){
            exist = false;
            System.out.print("Gebruikersnaam: ");
            inputNaam = scan.nextLine();
            System.out.println("Passwoord:");
            inputPasswoord = scan.nextLine();
            for(index = 0; index < 4; index++){
                //System.out.println(index);
                if (gebruiker[index].equals(inputNaam)){
                    exist = true;
                    break;
                }     
            }
            if (!exist){
                System.out.println("Logon mislukt!");
                continue;
            }
            if (!passwoord[index].equals(inputPasswoord)){
                System.out.println("Logon muslukt!");
                continue;
            }
            if (index == 4)
                break;
                
            System.out.format("Je bent ingelogd met prioriteit %d\n", prio[index]);
        }
    }
    
}
