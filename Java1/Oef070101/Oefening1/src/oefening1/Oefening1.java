/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int input, total = 0;
        
        System.out.print("Voer de eerste integer in (0 om te stoppen): ");
        input = scan.nextInt();
        if (input == 0)
        {
            System.out.println("\nGeen integers ingebracht\nTot ziens!");
            return;
        }
        total += input;
        
        while (input != 0)
        {
            System.out.print("Voer een integer in: ");
            input = scan.nextInt();
            total += input;
        }
        System.out.format("De som is: %d\n", total);
        
    }
    
}
