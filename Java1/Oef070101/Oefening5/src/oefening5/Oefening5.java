/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening5;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int x1, x2, y1, y2, breedte, hoogte, opp;
        
        for(;;){
            for(;;){
                System.out.print("Geef x1 en y1:\t");
                x1 = scan.nextInt();
                y1 = scan.nextInt();
                if (x1 < 0 || y1 < 0)
                    System.out.println("Foutive input, getallen moeten positief zijn, probeer nogmaals");
                else
                    break;
            }
            for(;;){
                System.out.print("Geef x2 en y2:\t");
                x2 = scan.nextInt();
                y2 = scan.nextInt();
                if (x2 < 0 || y2 < 0)
                    System.out.println("Foutive input, getallen moeten positief zijn, probeer nogmaals");
                else
                    break;
            }
            hoogte = y2 - y1;
            breedte = x2 - x1;
            //Tegengestelde bij negatief
            if (hoogte < 0)
                hoogte *= -1;
            if (breedte < 0)
                breedte *= -1;
            opp = hoogte * breedte;
            System.out.format("Breedte= %d Hoogte= %d Oppervlakte= %d\n", breedte, hoogte, opp);
            if (opp == 0)
                break;
        }
        System.out.println("Tot ziens!");
    }
    
}
