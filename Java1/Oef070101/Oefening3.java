/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code applcation logic here
        Scanner scan = new Scanner(System.in);
        int onderGrens = 0, bovenGrens = 0, binnen = 0, buiten = 0, input = 0;
        System.out.println("Voer ondergrens en bovengrens in:");
        onderGrens = scan.nextInt();
        bovenGrens = scan.nextInt();
        for (;;)
        {
            if (input == 0)  
                System.out.println("Voer eerste integer in (0 om te stoppen):");
            else
                System.out.println("Voer volgende integer in (0 om te stoppen):");
            input = scan.nextInt();
                
            if (input == 0)
                break;
            if ((input >= onderGrens) && (input <= bovenGrens))
                binnen += input;
            else
                buiten += input;
        }
        System.out.format("De som binnen bereik = %d\nDe som buiten bereik = %d\n", binnen, buiten);
    }
    
}
