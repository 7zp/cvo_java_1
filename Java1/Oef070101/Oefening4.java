/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Gilles
 */
public class Oefening4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double input, kost = 0;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        
        for(;;){
            System.out.println("Geef gewicht van pakje in kg (0 of minder om te stoppen):");
            input = scan.nextFloat();
            if(input <= 0)
                break;
            if (input <= 5){
                kost = 3;
            } else {
                kost = 3 + Math.ceil((input - 5)/0.5) * 0.25; 
            }
            System.out.format("De totale kost is %.2f\n", kost);
        }
        System.out.println("Tot ziens");
    }
    
}
