/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        int hoeveel, iter = 0;
        double input, som = 0, somSqr = 0, avg, avgSqr, avgSqrSomSqr, standardDeviation;
        
        System.out.println("Hoeveel getallen wil je inbrengen?");
        hoeveel = scan.nextInt();
        
        while (iter < hoeveel){
            System.out.print("Geef een getal: ");
            input = scan.nextDouble();
            
            som += input;
            somSqr += (input * input);
            
            iter++;
        }
        System.out.format("sommen van Xi = %f en Xi² = %f %n", som, somSqr);
        avg = som / hoeveel;
        avgSqr = (avg * avg);
        avgSqrSomSqr = somSqr / hoeveel;
        System.out.format("avg = %f avg³ = %f en gemiddelde van de som van de kwadraten = %f %n", avg, avgSqr, avgSqrSomSqr);
        standardDeviation = Math.sqrt(avgSqrSomSqr - avgSqr);
        System.out.format("Standaard afwijking is %f %n", standardDeviation);
        
    }
    
}
