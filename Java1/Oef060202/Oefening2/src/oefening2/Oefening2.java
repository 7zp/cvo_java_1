/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double hoeveel, teller = 0;
        
        System.out.println("Geef een geheel getal in: ");
        hoeveel = scan.nextDouble();
        
        while (hoeveel > 0){
            teller += 1/hoeveel; 
            hoeveel--;
        }
        System.out.format("Som is: %f %n", teller);
    }
    
}
