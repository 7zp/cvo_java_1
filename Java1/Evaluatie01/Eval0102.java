/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eval0102;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author van.vlasselaer
 */
public class Eval0102 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        float gewicht, begin, eind, nodig;
        
        
        System.out.println("Voer de hoeveelheid water in (in kg): ");
        gewicht = scan.nextFloat();
        System.out.println("Voer de begin temperatuur in: ");
        begin = scan.nextFloat();
        System.out.println("Voer de eind temperatuur in: ");
        eind = scan.nextFloat();
        
        //Bereken
        nodig = gewicht * (eind - begin ) * 4184;
        System.out.format("De benodigde energie is: %.1f\n", nodig);
    }
    
}
