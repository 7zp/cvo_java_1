/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eval0101;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author van.vlasselaer
 */
public class Eval0101 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double oppervlakte, straal, lengte, volume;
        Scanner scan = new Scanner(System.in).useLocale(Locale.ENGLISH);
        
        System.out.println("Geef de straal van de cilinder:");
        straal = scan.nextFloat();
        
        System.out.println("Geef de lengte van de cilinder:");
        lengte = scan.nextFloat();
        
        oppervlakte = straal * straal * Math.PI;
        volume = oppervlakte * lengte;
        
        System.out.format("Het volume van de cilinder is %.2f\n", oppervlakte, volume);
    }
    
}
