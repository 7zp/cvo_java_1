/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eval0105;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author van.vlasselaer
 */
public class Eval0105 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        double input, a, b, c, totaal;
        
        System.out.println("Voer een getal tussen 0 en 1000 in:");
        input = scan.nextFloat();

        
        a = Math.floor(input / 100);
        input =  input % 100;
        
        b = Math.floor(input / 10);
        c = input % 10;
        
        totaal = a + b + c;
        System.out.format("De some van de getallen is %.0f\n", totaal);
        
    }
    
}
