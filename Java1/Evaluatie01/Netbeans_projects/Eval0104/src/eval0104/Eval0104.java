/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eval0104;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author van.vlasselaer
 */
public class Eval0104 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        double sub, fooiperc, fooi, totaal;
        System.out.println("Voer subtotaal en fooi percentage in: ");
        sub = scan.nextFloat();
        fooiperc = scan.nextFloat();
        
        fooi = (sub / 100) * fooiperc;
        totaal = sub + fooi;
        
        System.out.format("De fooi is %.2f en het totaal bedrag is %.2f\n", fooi, totaal);
        
    }
    
}
