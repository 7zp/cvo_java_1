/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eval0103;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author van.vlasselaer
 */
public class Eval0103 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double investering, intrest, jaar, toekomstWaarde;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        
        System.out.println("Voer het investeringsbedrag in: ");
        investering = scan.nextFloat();
        System.out.println("Voer de jaarlijkse intrestvoet in: ");
        intrest = scan.nextFloat();
        System.out.println("Voer het aantal jaar in:");
        jaar = scan.nextFloat();
        
        toekomstWaarde = investering * Math.pow((1 + (intrest/100)), jaar);
        
        System.out.format("De geaccumuleerde bedrag is %.2f", toekomstWaarde);
        
        
        
    }
    
}
