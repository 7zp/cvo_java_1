/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        double schuld, percent, maandelijks, betaald=0, maand = 1;
        DecimalFormat form =  new DecimalFormat("###.##");
        System.out.println("Hoeveel bedraagt de schuld?");
        //schuld = scan.nextFloat();
        schuld = 1000;
        System.out.println("Hoeveel bedraagt de debetintrest in percent?");
        //percent = scan.nextFloat();
        percent = 1.5;
        System.out.println("Hoeveel wil je elke maand afbetalen?");
        //maandelijks = scan.nextFloat();
        maandelijks = 100;
        
        percent /= 100;
        while(schuld > 0){
            //intrest
            
            schuld += schuld*percent - maandelijks;
            if(schuld < 0)
                betaald = betaald + (maandelijks + schuld);
            else
                betaald += maandelijks;
            System.out.format("Maand %.0f:\tsaldo the betalen:\t€ %s\ttotaal terugbetalingen\t€ %s\n", maand, form.format(schuld), form.format(betaald));
            maand++;
        }
        
    }
    
}
