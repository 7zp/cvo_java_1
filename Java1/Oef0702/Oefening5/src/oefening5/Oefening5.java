/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening5;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        double n;
        
        System.out.println("Geef een willekeurig geheel getal in: ");
        n = scan.nextFloat();
        while (n > 1){
            if ( n % 2 == 0)
                n /= 2;
            else
                n = (3*n)+1;
            System.out.println((int)n);
        }
    }
    
}
