/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        final double goal = 1.0E-12;
        double x, som =0, term = 1, iter = 1, factorial =0, factotal =1;
        System.out.print("Geef x:");
        x = scan.nextFloat();
        som += x;
        while (term > goal){
            if (iter < 3){
                if(iter == 1){
                    term = 2;
                    som += +1;
                }else
                    som += term;
            }else{
            factorial = iter;
            factotal =1;
            while(factorial > 1){
                factotal = factotal * factorial;
                factorial -= 1;
                
            }
            term = Math.pow(x, iter) / factotal;
            som += term;
            }
            
            //System.out.format("Iteratie: %.0f\tterm: %.10f\tsom %f\n", iter, term, som);
            System.out.println("Iteratie: "+iter+"\tterm: "+term+"\tsom "+som);
            iter++;

        }
        System.out.println("Mijn e^x:\t"+som);
        System.out.println("Echte e^x\t"+Math.exp(x));
    }
    
}
