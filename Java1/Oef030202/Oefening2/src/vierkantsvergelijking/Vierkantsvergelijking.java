/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vierkantsvergelijking;

/**
 *
 * @author Gilles
 */
public class Vierkantsvergelijking {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double x;
        double resultaat;
        x = 7.62;
        resultaat = (3*(Math.pow(x, 2)) - (8*x) + 4);
        System.out.format("Bij x = %f is de waarde %f%n", x, resultaat);
    }
    
}
