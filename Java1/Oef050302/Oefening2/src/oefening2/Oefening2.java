/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Scanner;

public class Oefening2 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int spanningLinksVoor, spanningLinksAchter, spanningRechtsVoor, spanningRechtsAchter;
        
        System.out.println("Vul de bandenspanning in 1.Voor links 2.Voor rechts 3.Achter links 4.Achter rechts:");
        spanningLinksVoor = scan.nextInt();
        spanningRechtsVoor = scan.nextInt();
        spanningLinksAchter = scan.nextInt();
        spanningRechtsAchter = scan.nextInt();
        
        if ((spanningLinksVoor != spanningRechtsVoor) || (spanningLinksAchter != spanningRechtsAchter))
            System.out.println("Bandenspanning NIET OK");
        else
            System.out.println("Bandenspanning OK");
    }
    
}
