/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;
import java.util.Scanner;

public class Oefening4 {
    static Scanner scan = new Scanner(System.in);
    final private static int spanningMax = 45;
    final private static int spanningMin = 35;
    private static boolean spanningNietOk = false;
    
    private static int spanningInput(String band){
        System.out.format("Spanning band %s:%n", band);
        int spanning = scan.nextInt();
        if (spanning > spanningMax || spanning < spanningMin){
            System.out.println("Verwittiging! - Spanning buiten toegelaten bereik!");
            spanningNietOk = true;
        }
        return spanning;
    }
    public static void main(String[] args) {
        
        int spanningLinksVoor, spanningLinksAchter, spanningRechtsVoor, spanningRechtsAchter;
        
        spanningLinksVoor = spanningInput("vooraan links");
        spanningRechtsVoor = spanningInput("vooraan rechts");
        spanningLinksAchter = spanningInput("achteraan links");
        spanningRechtsAchter = spanningInput("achteraan rechts");
        
        if (spanningNietOk || ((spanningLinksVoor - spanningRechtsVoor) > 3) || ((spanningRechtsVoor - spanningLinksVoor) > 3) || ((spanningLinksAchter - spanningRechtsAchter) > 3) || ((spanningRechtsVoor - spanningRechtsAchter) > 3)) 
            System.out.println("Bandenspanning NIET OK");
        else
            System.out.println("Bandenspanning OK");
    }
    
}
