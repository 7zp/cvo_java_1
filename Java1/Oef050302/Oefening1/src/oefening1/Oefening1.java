/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {
    public static void main(String[] args) {
        int rekeningLopende, rekeningSpaar;
        double onkosten;
        Scanner scan = new Scanner(System.in);
        System.out.println("Geef saldo van 1. lopende rekening en 2. spaarrekening");
        rekeningLopende = scan.nextInt();
        rekeningSpaar = scan.nextInt();
        
        if (rekeningLopende > 1000 || rekeningSpaar > 1500)
            onkosten = 0;
        else
            onkosten = 0.15;
        
        System.out.format("De onkosten bedragen %.2f %n", onkosten);
    }
}
