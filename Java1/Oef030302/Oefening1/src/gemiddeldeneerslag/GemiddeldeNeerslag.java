/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemiddeldeneerslag;

/**
 *
 * @author Gilles
 */
public class GemiddeldeNeerslag {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Neerslag regen
        double sinus = Math.sin(0.85);
        double cosinus = Math.cos(0.32);
        double somKwadraten = (Math.pow(sinus, 2.0) + Math.pow(cosinus, 2.0));
        System.out.format("Sinus: %f; cosinus: %f, Som van hun kwadraten: %f%n", sinus, cosinus, somKwadraten);
    } 
}
