/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening5;
import java.util.Scanner;

public class Oefening5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        final int papierOppervlakte = 70 *500;
        int totaalMuurBreedte = 0, totaalMuurHoogte = 0, teNegerenBreedte = 0, teNegerenHoogte = 0, totaalOpperVlakte, input1, input2, papierAantal;
        double totaalMuurOpperVlakteMeter, totaalNegerenOpperVlakteMeter, totaalOppervlakteMeter;
        
        System.out.println("Geef de hoogte en breedte voor elke muur in centimeter, eenmaal je alle muren hebt ingevoerd voer 0 in om verder te gaan.");
        for(;;){
            System.out.println("Geef de dimensies van een muur 1. breedte 2. hoogte:");
            input1 = scan.nextInt();
            if (input1 == 0)
                break;
            input2 = scan.nextInt();
            if (input2 == 0)
                break;
            //Bereken oppervlakte
            totaalMuurBreedte += input1;
            totaalMuurHoogte += input2;
        }
        //Totale oppervlakte muren (met deuren, ramen, ...)
        totaalOpperVlakte = totaalMuurBreedte * totaalMuurHoogte;
        totaalMuurOpperVlakteMeter = totaalOpperVlakte / 10000;
        System.out.println("Geef de hoogte en breedte voor je ramen, deuren, plinten..., eenmaal je klaar bent voer 0 in om verder te gaan.");
        for(;;){
            System.out.println("Geef de dimensies van het de te negeren oppervlakte 1. breedte 2. hoogte:");
            input1 = scan.nextInt();
            if (input1 == 0)
                break;
            input2 = scan.nextInt();
            if (input2 == 0)
                break;
            //Bereken oppervlakte
            teNegerenBreedte += input1;
            teNegerenHoogte += input2;
        }
        //Verlaag de totaal oppervlakte
        totaalOpperVlakte -= teNegerenBreedte*teNegerenHoogte;
        totaalNegerenOpperVlakteMeter = (teNegerenBreedte * teNegerenHoogte) / 10000;
        totaalOppervlakteMeter = totaalOpperVlakte / 10000;
        //Bereken de rollen
        papierAantal = (int)Math.ceil(totaalOpperVlakte / papierOppervlakte);
        
        System.out.format("De totale oppervlakte van de muur is %.3fm² %n"
                + "De totale oppervlakte te negeren is %.3fm²%n"
                + "De totale oppervlakte te behangen is %.3fm²%n"
                + "Je hebt in totaal %d rollen behangpapier nodig.%n", 
                totaalMuurOpperVlakteMeter, totaalNegerenOpperVlakteMeter, totaalOppervlakteMeter, papierAantal);
    }
    
}
