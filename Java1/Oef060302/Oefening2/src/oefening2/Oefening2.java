/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Scanner;

public class Oefening2 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int input, somMacht2 = 0, somMacht3 = 0, kwadraat;
        
        System.out.println("Geef een integer:");
        input = scan.nextInt();
        
        while(input > 0) {
            kwadraat = (input * input); //Hoeft geen twee keer hetzelfde te doen
            somMacht2 += kwadraat;
            somMacht3 += (kwadraat * input);
            input--;
        }
        System.out.format("Som kwadraten %d %nSom derde machten %d%n", somMacht2, somMacht3);
        
    }
    
}
