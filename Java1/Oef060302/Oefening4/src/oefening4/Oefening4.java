/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;

import java.util.Scanner;
public class Oefening4 {


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int boomBreedte, boomStamBreedte, boomStamHoogte, tekenBegin = 1,tekenBeginPunt, tekenBreedte = 1, tekenMidden, boomStamBreedteCopy;
        double evenCheck;
        System.out.println("Geef de eigenschappen voor je boom 1. Breedte van de boom (oneven) 2. Breedte van de stam (oneven) 3. Hoogte van de stam:");
        boomBreedte = scan.nextInt();
        boomStamBreedte = scan.nextInt();
        boomStamHoogte = scan.nextInt();
        
        /*
        //Tests
        boomBreedte = 111;
        boomStamBreedte = 43;
        boomStamHoogte = 12;
        */
        evenCheck = boomBreedte % 2;
        if (evenCheck == 0){
            System.out.println("De breedte van de boom moet een oneven getal zijn!");
            return;
        }
        
        evenCheck = boomStamBreedte % 2;
        if (evenCheck == 0) {
            System.out.println("De breedte van de stam moet een oneven getal zijn!");
            return;
        }
        if (boomStamBreedte >= boomBreedte){
            System.out.println("Je stam mag niet breder zijn dan de boom zelf!");
            return;
        }
        
        //Newline
        System.out.println("");
        
        //Boom
        tekenMidden = boomBreedte / 2 ;
        while (tekenBegin <= boomBreedte) {
            //Bereken afstand voor spaties
            tekenBeginPunt = tekenMidden;
            while (tekenBeginPunt > 0){
                System.out.print(" ");
                tekenBeginPunt--;
            }
            //Voeg juiste aantal sterren toe
            while(tekenBreedte > 0){
                System.out.print("*");
                tekenBreedte--;
            }
            //Newline
            System.out.println("");
            tekenBreedte += tekenBegin+2;
            tekenMidden -= 1;
            tekenBegin += 2;
        }
        
        //Stam
        tekenMidden = boomBreedte / 2 ;
        while (boomStamHoogte > 0) {
            //Spaties
            tekenBeginPunt = tekenMidden - (boomStamBreedte / 2);
            while (tekenBeginPunt > 0){
                System.out.print(" ");
                tekenBeginPunt--;
            }
            //Stam sterren
            boomStamBreedteCopy = boomStamBreedte;
            while (boomStamBreedteCopy > 0 ){
                System.out.print("*");
                boomStamBreedteCopy--;
            }
            System.out.println("");
            boomStamHoogte--;
        }
    } 
}
