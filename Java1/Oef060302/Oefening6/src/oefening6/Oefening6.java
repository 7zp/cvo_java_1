package oefening6;

import java.util.Scanner;

class Oefening6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        long lFact = 1, n1 = 1, n2 = 1, times;
        double dFact = 1;
        while(true){
            lFact = lFact * n1;
            if (lFact < 0)  // Signed bit bereikt
                break;
            n1++;
        }
        while(true){
            dFact = dFact * n2;
            if (dFact < 0) // sgined bit check
                break;
            n2++;
        }
        
        n1--; //Max fac Long
        n2--; //        Double
        System.out.format("Maximale faculteit van Long is %d, van Double is %d%n", n1, n2);
        
    }
}
