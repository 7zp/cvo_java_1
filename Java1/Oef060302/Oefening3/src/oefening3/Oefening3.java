/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int aantalBegin, aantalSterren;
        
        System.out.println("Geef een begin aantal:");
        aantalBegin = scan.nextInt();
        
        while (aantalBegin > 0){
            aantalSterren = aantalBegin;
            while(aantalSterren > 0){
                System.out.print("*");
                aantalSterren--;
            }
            System.out.println(""); //Newline
            aantalBegin--;
        }
    }
    
}
