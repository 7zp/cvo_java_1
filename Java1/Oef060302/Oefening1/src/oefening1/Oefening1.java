/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
public class Oefening1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        double tijdReis, tijdZones, tijdHerstel, tijdVertrek, tijdAankomst;
        
        System.out.println("Geef aantal uren reistijd in, en aantal overschreden tijdzones, vertrek uur en aankomst uur:");
        tijdReis = scan.nextDouble();
        tijdZones = scan.nextDouble();
        tijdVertrek = scan.nextDouble();
        tijdAankomst = scan.nextDouble();
        
        tijdHerstel = (tijdReis / 2 + (tijdZones - 3 ) + tijdVertrek + tijdAankomst) / 10;
        
        System.out.format("Aantal dagen voor je jetlag te herstellen is: %f dag(en) %n", tijdHerstel);
    }
    
}
