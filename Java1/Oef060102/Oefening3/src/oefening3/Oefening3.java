/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Scanner;

public class Oefening3 {
    public static void main(String[] args) {
        int lengteTotaal = 0;
        Scanner scan = new Scanner(System.in);
        String woord1, woord2;
        
        System.out.println("Geef een woord:");
        woord1 = scan.nextLine();
        System.out.println("Geef nog een woord");
        woord2 = scan.nextLine();
        
        lengteTotaal = woord1.length() + woord2.length();
        System.out.print(woord1);
        
        while (lengteTotaal < 30 ){
            System.out.print(".");
            lengteTotaal++;
        }
        System.out.println(woord2);
        
    }
    
}
