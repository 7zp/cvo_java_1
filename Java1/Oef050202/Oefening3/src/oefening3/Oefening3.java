/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int stuks, tijd;
        double totaal;
        
        System.out.println("Geef achtereenvolgens het aantal stukken en de verwarmingstijd voor 1 stuk in seconden:");
        stuks = scan.nextInt();
        tijd = scan.nextInt();
        
        if (stuks == 1){
            System.out.format("Totale verwarmingstijd = %d%n", tijd);
            return;
        }
        if (stuks == 2){
            totaal = (double)(tijd * 1.5);
        } else if ( stuks == 3){
            totaal = (double)(tijd * 2);
        } else {
            System.out.println("Het is niet aan te raden om meer dan drie stukken tegelijk op te warmen!");
            totaal = (double)tijd;
        }
        System.out.format("Totale verwamingstijd = %.0f %n", totaal);
    }
}
