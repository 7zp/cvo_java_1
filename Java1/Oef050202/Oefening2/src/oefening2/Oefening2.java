/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;

import java.util.Scanner;
public class Oefening2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double tempLucht, tempStoom, rendement;
        
        System.out.println("Geef temperatuur lucht en temperatuur stoom in graden Kelvin:");
        tempLucht = scan.nextDouble();
        tempStoom = scan.nextDouble();
        
        if (tempStoom < 373){
            System.out.println("Het rendement is 0 (Er is geen stoom)");
            return;
        }
        
        rendement = 1 - (tempLucht / tempStoom);
        System.out.format("Het rendement is %f%n", rendement);
    }
}
