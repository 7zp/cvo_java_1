/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
public class Oefening1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String artikelNaam;
        int leveringGewenst;
        double totaal = 0, leveringPrijs = 0, artikelPrijs;
        
        final int leveringGewoon = 200, leveringMeer = 300, leveringSnel = 500;
        
        System.out.println("Gewenste artikel:");
        artikelNaam = scan.nextLine();
        System.out.println("Geef prijs artikel in eurocent:");
        artikelPrijs = scan.nextInt();
        System.out.println("Gewone levering (0) of Snelle levering (1)");
        leveringGewenst = scan.nextInt();
        
        //Levering prijs
        if (artikelPrijs > 1000)
            leveringPrijs += 300;
        else
            leveringPrijs += 200;
        
        //Levering methode
        if (leveringGewenst == 1)
            leveringPrijs += 500;
        
        leveringPrijs /= 100;
        artikelPrijs /= 100;
        
        totaal = (artikelPrijs + leveringPrijs);
        
        System.out.format("Artikel:\t€ %.2f %s%n", artikelPrijs, artikelNaam);
        System.out.format("Levering:\t€ %.2f %n", leveringPrijs);
        System.out.format("Totaal prijs:\t€ %.2f %n", totaal);  
    }    
}
