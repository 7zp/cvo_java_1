/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;

import java.util.Scanner;
public class Oefening4 {
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        String naam;
        int kracht, gezondheid, geluk;
        final int maxPunten = 15;
        
        System.out.println("Welkom bij het ons Grote Fantasiespel!");
        System.out.println("Geef de naam van je figuur:");
        naam = scan.nextLine();
        System.out.println("Geef na elkaar een waarde van 0-10 voor 'Kracht', 'Gezondheid' en 'Geluk'");
        kracht = scan.nextInt();
        gezondheid = scan.nextInt();
        geluk = scan.nextInt();
        
        if (    kracht      < 0      || kracht      > 10      ||
                gezondheid  < 0      || gezondheid  > 10      ||
                geluk       < 0      || gezondheid  > 10){
            System.out.println("Foutive waarden opgegeven. Je krijgt default waarden toegewezen.");
            kracht = 5;
            gezondheid = 5;
            geluk = 5;
        } else if ((kracht + gezondheid + geluk) > maxPunten) {
            System.out.println("Je hebt je figuur te veel punten gegeven! er worden defualt waarden gegeven");
            kracht = 5;
            gezondheid = 5;
            geluk = 5;
        }
        System.out.format("Figuur: %s Kracht: %d Gezondheid: %d Geluk: %d%n", naam, kracht, gezondheid, geluk);
    }
    
}
