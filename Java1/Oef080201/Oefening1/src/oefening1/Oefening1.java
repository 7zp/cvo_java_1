/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int klinkers = 0, medeklinkers = 0, tekens = 0, cijfers = 0, spaties = 0, stringLen, iter;
        String input;
        System.out.println("Voer een zin in:");
        input = scan.nextLine();
        stringLen = input.length();
        input = input.toLowerCase();
        for (iter = 0; iter < stringLen; iter++){
            switch (input.charAt(iter)){
                case '.': case ',': case '?': case '!':
                    tekens++;
                    continue;
                case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
                    cijfers++;
                    continue;
                case ' ':
                    spaties++;
                    continue;
                case 'a': case 'e': case 'i': case 'o': case 'u':
                    klinkers++;
                    continue;
                default:
                    medeklinkers++;
                    continue;
            }
        }
        System.out.format("Aantal klinkers %d\nAantal medeklinkers %d\nAantal tekens %d\nAantal cijfers %d\nAantal spaties %d\n", klinkers, medeklinkers, tekens, cijfers, spaties);
        
    }
    
}
