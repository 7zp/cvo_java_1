/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        int input, iteratie, som = 0, hoogste = 0;
        
        for(iteratie = 0; iteratie < 5; iteratie++){
            System.out.println("Voer een getal in: ");
            input = scan.nextInt();
            if (input > hoogste)
                hoogste = input;
            som += input;
        }
        System.out.format("Je hoogst ingegeven getal was %d\nDe som van de ingegeven getallen is %d\n", hoogste, som);
    }
    
}
