/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        double schapen = 20;
        int jaren;
        System.out.format("Kweekgroep van %.0f schapen\n", schapen);
        for (jaren = 0; jaren < 80; jaren++){
            schapen =  (220 / (1 + 10* Math.pow(0.83, jaren)));
            if(jaren == 0)
                continue;
            System.out.format("Na %d jaar heb je %.2f schapen.\n", jaren, schapen);
            if ( schapen >= 80)
                break;
        }
    }
    
}
