/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening4;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double x, y, h1, h2;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        
        System.out.println("Voer twee getallen in, gescheiden door een spatie:");
        x = scan.nextDouble();
        y = scan.nextDouble();
        
        h1 = (2 / ((1/x) + (1/y)));
        h2 = ((x + y) / 2);
        
        System.out.format("Harmonisch gemiddelde is %f %nRekenkundig gemiddelde is %f %n", h1, h2);
    }
    
}
