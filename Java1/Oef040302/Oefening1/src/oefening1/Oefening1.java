/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening1;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double prijs, verbruik, totaal;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        System.out.println("Voer de prijs per kwu in cent in:");
        prijs = scan.nextDouble();
        System.out.println("Voer het jaar verbruik in:");
        verbruik = scan.nextDouble();
        
        totaal = (prijs * verbruik) / 100;
        
        System.out.format("Jaarlijkse kost: %.2f€ %n", totaal);
        
    }
    
}
