/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening2;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final double versnelling = 9.81;    //Is eigenlijk geen constante
        double afstand;
        int tijd;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        System.out.println("Vul het aantal seconden van de val in:");
        tijd = scan.nextInt();
        
        afstand = (versnelling * (double)(tijd * tijd)) / 2;
        
        System.out.format("Totale gevallen afstand is: %.2f meter%n", afstand);
    }
    
}
