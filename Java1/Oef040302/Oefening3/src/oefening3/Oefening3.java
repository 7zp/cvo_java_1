/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oefening3;
import java.util.Locale;
import java.util.Scanner;
/**
 *
 * @author Gilles
 */
public class Oefening3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double x, antwoord;
        Scanner scan = new Scanner(System.in).useLocale(Locale.UK);
        
        System.out.println("Voer een getal in:");
        x = scan.nextDouble();
        //x = 998.65;
        
        antwoord = (Math.log(x) / Math.log(2));
        
        System.out.format("Logaritme basis 2 van %.2f is %f %n", x, antwoord);
    }
    
}
